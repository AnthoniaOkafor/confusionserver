const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Favorites = require('../models/favorite');

const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.find({ user: req.user._id })
    .populate('user')
    .populate('dishes')
    .then((favorites) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
   
    }, (err) => next(err))
    .catch((err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({ user: req.user._id })
        .then((favorites) => {
            if (favorites != null) {
                for (i=0; i<req.body.length; i++) {
                    if (favorites.dishes.includes(req.body[i]._id)) {
                        console.log('Dish is already included in the list of favorites');
                        err = new Error('Dish ' + req.body[i]._id + ' already included in the list of favorites');
                        err.status = 500;
                        return next(err);
                    }
                    else {
                        favorites.dishes.push(req.body[i]._id);
                        favorites.save();
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorites);
                    }
                }
            }
            else {
                Favorites.create({user: req.user._id, dishes: req.params.dishId})
                .then((favorites) => {
                    console.log('Favorite Created ', favorites);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                }, (err) => next(err))
            }
        }, (err) => next(err))
        .catch((err) => next(err))          
})
    
.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorites');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.remove({ user: req.user._id })
    .then((favorites) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));    
})


favoriteRouter.route('/:dishId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.find({ user: req.user._id })
    .populate('dishes')
    .then((favorite) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorite);
    }, (err) => next(err))
    .catch((err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({ user: req.user._id })
    .then((favorites) => {
        if (favorites != null) {
                if (favorites.dishes.includes(req.params.dishid)) {
                    console.log('Dish is already included in the list of favorites');
                    err = new Error('Dish ' + req.body[i]._id + ' already included in the list of favorites');
                    err.status = 500;
                    return next(err);
                }
                else {
                    favorites.dishes.push(req.params.dishid);
                    favorites.save();
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                }
        }
        else {
            Favorites.create({user: req.user._id, dishes: req.params.dishId})
            .then((favorites) => {
                console.log('Favorite Created ', favorites);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorites);
            }, (err) => next(err))
        }
    }, (err) => next(err))
    .catch((err) => next(err))          
})

.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorites/' + req.params.dishId);
})


/*.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findById(req.params.dishId)
    .then((favorite) => {
        if(favorite != null && favorite.dishes != null) {
            for (i=0; i <favorite.dishes.length; i++) {
                favorite.dishes.id(req.params.dishId).remove();
            }
            favorite.save();
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(favorite);
        }
        else if (favorite == null) {
            err = new Error('No list of favorites found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Dish' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);    
        }

    }, (err) => next(err))
    .catch((err) => next(err));
}); */

.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOneAndUpdate({user: req.user._id},
    {
        $pull: {dishes: req.params.dishId}
    }, 
    { new: true }
)
    .then((result) => {

        //if this user has added no favorites
        if(!result){
            res.statusCode = 403;
            res.end('You have not added any favorites');
        } 
        else{
            result.save()
            .then(() => {
                Favorites.findOne({user: req.user._id})
                .then((result) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({status: req.params.dishId + " removed from favorites.\n",result});
                }, (err) => next(err));  
            }, (err) => next(err));
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});


module.exports = favoriteRouter;

/*

/*.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findById(req.params.dishId)
    .then((favorite) => {
        if(favorite != null && favorite.dishes != null) {
            for (i=0; i <favorite.dishes.length; i++) {
                favorite.dishes.id(req.params.dishId).remove();
            }
            favorite.save();
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(favorite);
        }
        else if (favorite == null) {
            err = new Error('No list of favorites found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Dish' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);    
        }

    }, (err) => next(err))
    .catch((err) => next(err));
}); */



/*.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOneAndUpdate({user: req.user._id},
    {
        $pull: {dishes: req.params.dishId}
    }, 
    { new: true }
)
    .then((result) => {

        //if this user has added no favorites
        if(!result){
            res.statusCode = 403;
            res.end('You have not added any favorites');
        } 
        else{
            result.save()
            .then(() => {
                Favorites.findOne({user: req.user._id})
                .then((result) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json({status: req.params.dishId + " removed from favorites.\n",result});
                }, (err) => next(err));  
            }, (err) => next(err));
        }
    }, (err) => next(err))
    .catch((err) => next(err));
});


.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({user: req.user._id})
    .then((favorite) => {
        if(favorite != null && favorite.dishes != null) {
            for (i=0; i <favorite.dishes.length; i++) {
                favorite.dishes[i]._id(req.params.dishId).remove();
            }
            favorite.save();
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(favorite);
        }
        else if (favorite === null) {
            err = new Error('No list of favorites found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Dish' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);    
        }

    }, (err) => next(err))
    .catch((err) => next(err));
});
*/